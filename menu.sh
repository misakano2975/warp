# 当前脚本版本号和新增功能
VERSION=2.06
TXT="1) Add automatic check whether the Tun module is turned on; 2) Improve script adaptability, support hax VPS"

help(){
	yellow "warp h (help menu)\n warp o (temporary warp switch)\n warp u (uninstall warp)\n warp b (upgrade kernel, enable BBR and DD)\n warp d (free WARP account upgrade WARP+)\n warp d N5670ljg-sS9jD334-6o6g4M9F (specify the license to upgrade Warp+)\n warp p (flash WARP+ traffic)\n warp v (synchronize script to the latest version)\n warp 1 (Warp single stack)\n warp 1 N5670ljg-sS9jD334-6o6g4M9F (Specify Warp+ License Warp single stack)\n warp 2 (Warp dual stack)\n warp 2 N5670ljg-sS9jD334-6o6g4M9F (Specify Warp+ License Warp dual stack)\n" 
	}

# 字体彩色
red(){
	echo -e "\033[31m\033[01m$1\033[0m"
    }
green(){
	echo -e "\033[32m\033[01m$1\033[0m"
    }
yellow(){
	echo -e "\033[33m\033[01m$1\033[0m"
}

# 必须以root运行脚本
[[ $(id -u) != 0 ]] && red "The script must be run as root, you can enter sudo -i and then download and run again" && exit 1

# 必须加载 Tun 模块
TUN=$(cat /dev/net/tun 2>&1 | tr A-Z a-z)
[[ $TUN =~ 'not permit' ]] && red "The Tun module is not loaded, please open it in the VPS console or contact the supplier to learn how to open it" && exit 1

# 判断是否大陆 VPS，如连不通 CloudFlare 的 IP，则 WARP 项目不可用
ping -c1 -W1 2606:4700:d0::a29f:c001 >/dev/null 2>&1 && IPV6=1 && CDN=-6 || IPV6=0
ping -c1 -W1 162.159.192.1 >/dev/null 2>&1 && IPV4=1 && CDN=-4 || IPV4=0
if [[ $IPV4$IPV6 = 00 ]]; then
	if [[ -n $(wg) ]]; then
		wg-quick down wgcf >/dev/null 2>&1
		kill $(pgrep -f wireguard) >/dev/null 2>&1
		kill $(pgrep -f boringtun) >/dev/null 2>&1
		ping -c1 -W1 2606:4700:d0::a29f:c001 >/dev/null 2>&1 && IPV6=1 && CDN=-6 || IPV6=0
		ping -c1 -W1 162.159.192.1 >/dev/null 2>&1 && IPV4=1 && CDN=-4 || IPV4=0
	else
		red "Can’t connect to the WARP server. It may be a mainland VPS. You can manually ping 162.159.192.1 and 2606:4700:d0::a29f:c001. If you can connect, you can run the script again."
		exit 1
	fi
fi

# 判断操作系统，只支持 Debian、Ubuntu 或 Centos,如非上述操作系统，删除临时文件，退出脚本
SYS=$(hostnamectl | grep -i system | cut -d : -f2)
[[ -n $SYS ]] || SYS=$(cat /etc/redhat-release)
[[ -n $SYS ]] || SYS=$(cat /etc/issue | cut -d '\' -f1 | sed '/^[ ]*$/d')
[[ $(echo $SYS | tr A-Z a-z) =~ debian ]] && SYSTEM=debian
[[ $(echo $SYS | tr A-Z a-z) =~ ubuntu ]] && SYSTEM=ubuntu
[[ $(echo $SYS | tr A-Z a-z) =~ centos|kernel ]] && SYSTEM=centos
[[ $(echo $SYS | tr A-Z a-z) =~ 'amazon linux' ]] && SYSTEM=centos && COMPANY=amazon
[[ -z $SYSTEM ]] && red "This script only supports Debian, Ubuntu or CentOS systems" && exit 1

green "Checking VPS..."

# 安装 curl
[[ ! $(type -P curl) ]] && 
( yellow " Installing curl…… " && (apt -y install curl >/dev/null 2>&1 || yum -y install curl >/dev/null 2>&1) || 
( yellow " It must upgrade the software library before you can continue to install curl. It takes a long time, please be patient…… " && apt -y update >/dev/null 2>&1 && apt -y install curl >/dev/null 2>&1 || 
( yum -y update >/dev/null 2>&1 && yum -y install curl >/dev/null 2>&1 || ( yellow "Failed to install curl, the script aborted" && exit 1 ))))

# 判断处理器架构
[[ $(arch | tr A-Z a-z) =~ aarch ]] && ARCHITECTURE=arm64 || ARCHITECTURE=amd64

# 判断虚拟化，选择 Wireguard内核模块 还是 Wireguard-Go/BoringTun
[[ $(systemd-detect-virt | tr A-Z a-z) =~ openvz|lxc ]] && LXC=1
[[ $LXC = 1 && -e /usr/bin/boringtun ]] && UP='WG_QUICK_USERSPACE_IMPLEMENTATION=boringtun WG_SUDO=1 wg-quick up wgcf' || UP='wg-quick up wgcf'
[[ $LXC = 1 && -e /usr/bin/boringtun ]] && DOWN='wg-quick down wgcf && kill $(pgrep -f boringtun)' || DOWN='wg-quick down wgcf'

# 判断当前 IPv4 与 IPv6 ，归属 及 WARP 是否开启
[[ $IPV4 = 1 ]] && LAN4=$(ip route get 162.159.192.1 2>/dev/null | grep -oP 'src \K\S+') &&
		WAN4=$(curl -s4  https://ip.gs) &&
		COUNTRY4=$(curl -s4 https://ip.gs/country) &&
		TRACE4=$(curl -s4 https://www.cloudflare.com/cdn-cgi/trace | grep warp | cut -d= -f2)				
[[ $IPV6 = 1 ]] && LAN6=$(ip route get 2606:4700:d0::a29f:c001 2>/dev/null | grep -oP 'src \K\S+') &&
		WAN6=$(curl -s6  https://ip.gs) &&
		COUNTRY6=$(curl -s6 https://ip.gs/country) &&
		TRACE6=$(curl -s6 https://www.cloudflare.com/cdn-cgi/trace | grep warp | cut -d= -f2)

# 判断当前 WARP 状态，决定变量 PLAN，变量 PLAN 含义：1=单栈,	2=双栈,	3=WARP已开启
[[ $TRACE4 = plus || $TRACE4 = on || $TRACE6 = plus || $TRACE6 = on ]] && PLAN=3 || PLAN=$(($IPV4+$IPV6))

# 在KVM的前提下，判断 Linux 版本是否小于 5.6，如是则安装 wireguard 内核模块，变量 WG=1。由于 linux 不能直接用小数作比较，所以用 （主版本号 * 100 + 次版本号 ）与 506 作比较
[[ $LXC != 1 && $(($(uname  -r | cut -d . -f1) * 100 +  $(uname  -r | cut -d . -f2))) -lt 506 ]] && WG=1

# WGCF 配置修改，其中用到的 162.159.192.1 和 2606:4700:d0::a29f:c001 均是 engage.cloudflareclient.com 的IP
MODIFYS01='sed -i "/\:\:\/0/d" wgcf-profile.conf && sed -i "s/engage.cloudflareclient.com/[2606:4700:d0::a29f:c001]/g" wgcf-profile.conf'
MODIFYD01='sed -i "7 s/^/PostUp = ip -6 rule add from '$LAN6' lookup main\n/" wgcf-profile.conf && sed -i "8 s/^/PostDown = ip -6 rule delete from '$LAN6' lookup main\n/" wgcf-profile.conf && sed -i "s/engage.cloudflareclient.com/[2606:4700:d0::a29f:c001]/g" wgcf-profile.conf && sed -i "s/1.1.1.1/1.1.1.1,9.9.9.9,8.8.8.8/g" wgcf-profile.conf'
MODIFYS10='sed -i "/0\.\0\/0/d" wgcf-profile.conf && sed -i "s/engage.cloudflareclient.com/162.159.192.1/g" wgcf-profile.conf && sed -i "s/1.1.1.1/9.9.9.9,8.8.8.8,1.1.1.1/g" wgcf-profile.conf'
MODIFYD10='sed -i "7 s/^/PostUp = ip -4 rule add from '$LAN4' lookup main\n/" wgcf-profile.conf && sed -i "8 s/^/PostDown = ip -4 rule delete from '$LAN4' lookup main\n/" wgcf-profile.conf && sed -i "s/engage.cloudflareclient.com/162.159.192.1/g" wgcf-profile.conf && sed -i "s/1.1.1.1/9.9.9.9,8.8.8.8,1.1.1.1/g" wgcf-profile.conf'
MODIFYD11='sed -i "7 s/^/PostUp = ip -4 rule add from '$LAN4' lookup main\n/" wgcf-profile.conf && sed -i "8 s/^/PostDown = ip -4 rule delete from '$LAN4' lookup main\n/" wgcf-profile.conf && sed -i "9 s/^/PostUp = ip -6 rule add from '$LAN6' lookup main\n/" wgcf-profile.conf && sed -i "10 s/^/PostDown = ip -6 rule delete from '$LAN6' lookup main\n/" wgcf-profile.conf && sed -i "s/engage.cloudflareclient.com/162.159.192.1/g" wgcf-profile.conf && sed -i "s/1.1.1.1/9.9.9.9,8.8.8.8,1.1.1.1/g" wgcf-profile.conf'

# 由于warp bug，有时候获取不了ip地址，加入刷网络脚本手动运行，并在定时任务加设置 VPS 重启后自动运行,i=当前尝试次数，j=要尝试的次数
net(){
	[[ ! $(type -P wg-quick) || ! -e /etc/wireguard/wgcf.conf ]] && red "WireGuard tools are not installed or the configuration file wgcf.conf cannot be found, please reinstall" && exit 1 ||
	i=1;j=10
	yellow "During WARP registration, the maximum number of attempts is $j..."
	yellow "Attempting for $i time"
	echo $UP | sh >/dev/null 2>&1
	WAN4=$(curl -s4m10 https://ip.gs) &&
	WAN6=$(curl -s6m10 https://ip.gs)
	until [[ -n $WAN4 && -n $WAN6 ]]
		do	let i++
			yellow "Attempting for $i times"
			echo $DOWN | sh >/dev/null 2>&1
			echo $UP | sh >/dev/null 2>&1
			WAN4=$(curl -s4m10 https://ip.gs) &&
			WAN6=$(curl -s6m10 https://ip.gs)
			[[ $i = $j ]] && (echo $DOWN | sh >/dev/null 2>&1; red "Failed more than $i times, the script is aborted") && exit 1
        	done
green "WARP network has been successfully installed\n IPv4:$WAN4\n IPv6:$WAN6"
	}

# WARP 开关
onoff(){
	[[ -n $(wg) ]] 2>/dev/null && (echo $DOWN | sh >/dev/null 2>&1; green "WARP has been suspended, you can use warp to start it again. o") || net
	}

# VPS 当前状态
status(){
	clear
	yellow "This project specifically adds wgcf network interface for VPS\nScript features:\n * Support Warp+ account, with a third-party flashing Warp+ traffic and upgrading the kernel BBR Scripts\n * Ordinary user-friendly menus, advanced ones can quickly build through the suffix options\n * Intelligent judgment of vps operating system: Ubuntu 18.04, Ubuntu 20.04, Debian 10, Debian 11, CentOS 7, CentOS 8, please be sure to choose LTS system ;Intelligent judgment of the hardware structure type: AMD or ARM\n * Combined with the Linux version and virtualization mode, three WireGuard schemes are automatically selected. Network performance: Kernel integration WireGuard＞Kernel module installation＞boringtun＞wireguard-go\n * Smart judgment The latest release of the WGCF author's github library (Latest release)\n * Intelligent analysis of intranet and public network IP to generate WGCF configuration file\n * Output of execution results, prompt whether to use WARP IP, IP attribution\n"
	red "=============================================== ================================================== ====================\n"
	green "Script version: $VERSION New features: $TXT\n System information:\n Current operating system: $SYS\n Kernel: $(uname -r) \n Processor architecture: $ARCHITECTURE\n Virtualization: $(systemd-detect-virt) "
	[[ $TRACE4 = plus ]] && green "IPv4: $WAN4 (WARP+ IPv4) $COUNTRY4"
	[[ $TRACE4 = on ]] && green "IPv4: $WAN4 (WARP IPv4) $COUNTRY4"
	[[ $TRACE4 = off ]] && green "IPv4: $WAN4 $COUNTRY4"
	[[ $TRACE6 = plus ]] && green "IPv6: $WAN6 (WARP+ IPv6) $COUNTRY6"
	[[ $TRACE6 = on ]] && green "IPv6: $WAN6 (WARP IPv6) $COUNTRY6"
	[[ $TRACE6 = off ]] && green "IPv6: $WAN6 $COUNTRY6"
	[[ $TRACE4 = plus || $TRACE6 = plus ]] && green "WARP+ is turned on Device name: $(grep name /etc/wireguard/info.log 2>/dev/null | awk'{ print $NF }' ) "
	[[ $TRACE4 = on || $TRACE6 = on ]] && green "WARP is on"
	[[ $TRACE4 = off && $TRACE6 = off ]] && green "WARP is not turned on"
	 red "\n============================================= ================================================== ======================\n"
	}

# WGCF 安装
install(){
	# 脚本开始时间
	start=$(date +%s)
	
	# 输入 Warp+ 账户（如有），限制位数为空或者26位以防输入错误
	[[ -z $LICENSE ]] && read -p "If you have Warp+ key, please enter it, or press enter to continue:" LICENSE
	i=5
	until [[ -z $LICENSE || ${#LICENSE} = 26 ]]
		do
			let i--
			[[ $i = 0 ]] && red "Input errors up to 5 times, the script exits" && exit 1 || read -p "The key should be 26 characters, please re-enter the Warp+ key, or press enter to continue ($i times remaining):" LICENSE
		done

	# OpenVZ / LXC 选择 Wireguard-GO 或者 BoringTun 方案，并重新定义相应的 UP 和 DOWN 指令
	[[ $LXC = 1 ]] && read -p "LXC or OpenVZ plan: 1. Wireguard-GO or 2. BoringTun (the default value option is 1), please select:" BORINGTUN
	[[ $BORINGTUN = 2 ]] && UP='WG_QUICK_USERSPACE_IMPLEMENTATION=boringtun WG_SUDO=1 wg-quick up wgcf' || UP='wg-quick up wgcf'
	[[ $BORINGTUN = 2 ]] && DOWN='wg-quick down wgcf && kill $(pgrep -f boringtun)' || DOWN='wg-quick down wgcf'
	[[ $BORINGTUN = 2 ]] && WB=boringtun || WB=wireguard-go
	
	green "Progress 1/3: Install system dependencies"
	
	# 先删除之前安装，可能导致失败的文件，添加环境变量
	rm -rf /usr/local/bin/wgcf /usr/bin/boringtun /usr/bin/wireguard-go wgcf-account.toml wgcf-profile.conf
	[[ $PATH =~ /usr/local/bin ]] || export PATH=$PATH:/usr/local/bin
	
        # 根据系统选择需要安装的依赖
	debian(){
		# 更新源
		apt -y update

		# 添加 backports 源,之后才能安装 wireguard-tools 
		apt -y install lsb-release
		echo "deb http://deb.debian.org/debian $(lsb_release -sc)-backports main" > /etc/apt/sources.list.d/backports.list

		# 再次更新源
		apt -y update

		# 安装一些必要的网络工具包和wireguard-tools (Wire-Guard 配置工具：wg、wg-quick)
		apt -y --no-install-recommends install net-tools iproute2 openresolv dnsutils wireguard-tools

		# 如 Linux 版本低于5.6并且是 kvm，则安装 wireguard 内核模块
		[[ $WG = 1 ]] && apt -y --no-install-recommends install linux-headers-$(uname -r) && apt -y --no-install-recommends install wireguard-dkms
		}
		
	ubuntu(){
		# 更新源
		apt -y update

		# 安装一些必要的网络工具包和 wireguard-tools (Wire-Guard 配置工具：wg、wg-quick)
		apt -y --no-install-recommends install net-tools iproute2 openresolv dnsutils wireguard-tools
		}
		
	centos(){
		# 安装一些必要的网络工具包和wireguard-tools (Wire-Guard 配置工具：wg、wg-quick)
		[[ $COMPANY = amazon ]] && yum -y upgrade && amazon-linux-extras install -y epel
		yum -y install epel-release
		yum -y install net-tools wireguard-tools

		# 如 Linux 版本低于5.6并且是 kvm，则安装 wireguard 内核模块
		[[ $WG = 1 ]] && curl -Lo /etc/yum.repos.d/wireguard.repo https://copr.fedorainfracloud.org/coprs/jdoss/wireguard/repo/epel-7/jdoss-wireguard-epel-7.repo &&
		yum -y install wireguard-dkms

		# 升级所有包同时也升级软件和系统内核
		yum -y update
		}

	$SYSTEM

	# 安装并认证 WGCF
	green "Progress 2/3: Install WGCF"

	# 判断 wgcf 的最新版本,如因 github 接口问题未能获取，默认 v2.2.9
	latest=$(wget --no-check-certificate -qO- -T1 -t1 $CDN "https://api.github.com/repos/ViRb3/wgcf/releases/latest" | grep "tag_name" | head -n 1 | cut -d : -f2 | sed 's/\"//g;s/v//g;s/,//g;s/ //g')
	[[ -z $latest ]] && latest='2.2.9'

	# 安装 wgcf，尽量下载官方的最新版本，如官方 wgcf 下载不成功，将使用 jsDelivr 的 CDN，以更好的支持双栈。并添加执行权限
	wget --no-check-certificate -T1 -t1 -N $CDN -O /usr/local/bin/wgcf https://github.com/ViRb3/wgcf/releases/download/v$latest/wgcf_${latest}_linux_$ARCHITECTURE
	[[ $? != 0 ]] && wget --no-check-certificate -N $CDN -O /usr/local/bin/wgcf https://cdn.jsdelivr.net/gh/fscarmen/warp/wgcf_${latest}_linux_$ARCHITECTURE
	chmod +x /usr/local/bin/wgcf

	# 如是 LXC，安装 Wireguard-GO 或者 BoringTun 
	[[ $LXC = 1 ]] && wget --no-check-certificate -N $CDN -P /usr/bin https://cdn.jsdelivr.net/gh/fscarmen/warp/$WB && chmod +x /usr/bin/$WB

	# 注册 WARP 账户 (将生成 wgcf-account.toml 文件保存账户信息)
	yellow "WGCF registring..."
	until [[ -e wgcf-account.toml ]]
	  do
	   echo | wgcf register >/dev/null 2>&1
	done
	
	# 如有 Warp+ 账户，修改 license 并升级，并把设备名等信息保存到 /etc/wireguard/info.log
	mkdir -p /etc/wireguard/ >/dev/null 2>&1
	[[ -n $LICENSE ]] && yellow "Upgrade to Warp+ account" && sed -i "s/license_key.*/license_key = \"$LICENSE\"/g" wgcf-account.toml &&
	( wgcf update > /etc/wireguard/info.log 2>&1 || red "Upgrade fails, the Warp+ account is wrong or more than 5 devices have been activated, the free Warp account will be automatically replaced to continue" )
	
	# 生成 Wire-Guard 配置文件 (wgcf-profile.conf)
	wgcf generate >/dev/null 2>&1

	# 修改配置文件
	echo $MODIFY | sh

	# 把 wgcf-profile.conf 复制到/etc/wireguard/ 并命名为 wgcf.conf
	cp -f wgcf-profile.conf /etc/wireguard/wgcf.conf >/dev/null 2>&1

	# 自动刷直至成功（ warp bug，有时候获取不了ip地址），重置之前的相关变量值，记录新的 IPv4 和 IPv6 地址和归属地
	green "Progress 3/3: Run WGCF"
	unset WAN4 WAN6 COUNTRY4 COUNTRY6 TRACE4 TRACE6
	net
	COUNTRY4=$(curl -s4 https://ip.gs/country)
	TRACE4=$(curl -s4 https://www.cloudflare.com/cdn-cgi/trace | grep warp | cut -d= -f2)
	COUNTRY6=$(curl -s6 https://ip.gs/country)
	TRACE6=$(curl -s6 https://www.cloudflare.com/cdn-cgi/trace | grep warp | cut -d= -f2)

	# 设置开机启动
	systemctl enable wg-quick@wgcf >/dev/null 2>&1
	grep -qE '^@reboot[ ]*root[ ]*warp[ ]*n' /etc/crontab || echo '@reboot root warp n' >> /etc/crontab

	# 优先使用 IPv4 网络
	[[ -e /etc/gai.conf ]] && [[ $(grep '^[ ]*precedence[ ]*::ffff:0:0/96[ ]*100' /etc/gai.conf) ]] || echo 'precedence ::ffff:0:0/96  100' >> /etc/gai.conf

	# 保存好配置文件
	mv -f wgcf-account.toml wgcf-profile.conf menu.sh /etc/wireguard >/dev/null 2>&1
	
	# 创建再次执行的软链接快捷方式，再次运行可以用 warp 指令
	chmod +x /etc/wireguard/menu.sh >/dev/null 2>&1
	ln -sf /etc/wireguard/menu.sh /usr/bin/warp && green "Create shortcut warp successfully"

	# 结果提示，脚本运行时间
	red "\n==============================================================\n"
	[[ $TRACE4 = plus ]] && green " IPv4：$WAN4 ( WARP+ IPv4 ) $COUNTRY4 "
	[[ $TRACE4 = on ]] && green " IPv4：$WAN4 ( WARP IPv4 ) $COUNTRY4 "
	[[ $TRACE4 = off || -z $TRACE4 ]] && green " IPv4：$WAN4 $COUNTRY4 "
	[[ $TRACE6 = plus ]] && green " IPv6：$WAN6 ( WARP+ IPv6 ) $COUNTRY6 "
	[[ $TRACE6 = on ]] && green " IPv6：$WAN6 ( WARP IPv6 ) $COUNTRY6 "
	[[ $TRACE6 = off || -z $TRACE6 ]] && green " IPv6：$WAN6 $COUNTRY6 "
	end=$(date +%s)
	[[ $TRACE4 = plus || $TRACE6 = plus ]] && green "Congratulations! WARP+ has been turned on. Total time spent: $(( $end-$start )) seconds\n Device name: $(grep name /etc/ wireguard/info.log | awk'{ print $NF }')\n Remaining traffic: $(grep Quota /etc/wireguard/info.log | awk'{ print $(NF-1), $NF }') "
	[[ $TRACE4 = on || $TRACE6 = on ]] && green "Congratulations! WARP is turned on, total time spent: $(( $end-$start )) seconds "
	red "\n============================================= ================\n"
	yellow "To run again, use warp [option] [lisence], such as \n" && help
	[[ $TRACE4 = off && $TRACE6 = off ]] && red "WARP installation failed"
	}

# 关闭 WARP 网络接口，并删除 WGCF
uninstall(){
	unset WAN4 WAN6 COUNTRY4 COUNTRY6
	systemctl disable wg-quick@wgcf >/dev/null 2>&1
	echo $DOWN | sh >/dev/null 2>&1
	[[ $SYSTEM = centos ]] && yum -y autoremove wireguard-tools wireguard-dkms 2>/dev/null || apt -y autoremove wireguard-tools wireguard-dkms 2>/dev/null
	rm -rf /usr/local/bin/wgcf /etc/wireguard /usr/bin/boringtun /usr/bin/wireguard-go wgcf-account.toml wgcf-profile.conf /usr/bin/warp
	[[ -e /etc/gai.conf ]] && sed -i '/^precedence[ ]*::ffff:0:0\/96[ ]*100/d' /etc/gai.conf
	sed -i '/warp[ ]n/d' /etc/crontab
	WAN4=$(curl -s4m10 https://ip.gs)
	WAN6=$(curl -s6m10 https://ip.gs)
	COUNTRY4=$(curl -s4m10 https://ip.gs/country)
	COUNTRY6=$(curl -s6m10 https://ip.gs/country)
	[[ -z $(wg) ]] >/dev/null 2>&1 && green "WGCF has been completely deleted!\n IPv4: $WAN4 $COUNTRY4\n IPv6: $WAN6 $COUNTRY6" || red "is not cleared , Please reboot and try to delete again "
	}

# 安装BBR
bbrInstall() {
	red "\n============================================= ================"
	green "Mature works of [ylx2016] for BBR and DD scripts, address [https://github.com/ylx2016/Linux-NetSpeed], please be familiar with"
	yellow "1. Installation script [Recommend original BBR+FQ]"
	yellow "2. Back to home directory"
	red "=============================================== =============="
	read -p "Please select:" BBR
	case "$BBR" in
		1) wget --no-check-certificate -N "https://raw.githubusercontent.com/ylx2016/Linux-NetSpeed/master/tcp.sh" && chmod +x tcp.sh && ./tcp.sh;;
		2) menu$PLAN;;
		*) red "Please enter the correct number [1-2] "; sleep 1; bbrInstall;;
	esac
	}


# 刷 Warp+ 流量
input() {
	read -p "Please enter Warp+ key:" ID
	i=5
	until [[ ${#ID} = 36 ]]
		do
		let i--
		[[ $i = 0 ]] && red "Enter the wrong 5 times, the script exits" && exit 1 || read -p" Warp+ key should be 36 characters, please re-enter Warp+ key ($i times remaining): " ID
	done
	}

plus() {
	red "\n=============================================================="
	green "To get Warp+ traffic, you can choose the mature works of the following two authors. Please be familiar with:\n * [ALIILAPRO], address [https://github.com/ALIILAPRO/warp-plus-cloudflare]\n * [mixool], address [https://github.com/mixool/across/tree/master/wireguard]\n Download address: https://1.1.1.1/, visit and take care of the ID of Apple Outer Area\n Get Warp+ ID and fill in below. Method: Menu 3 in the upper right corner of the App --> Advanced --> Diagnosis --> ID\n Important: After the script is refreshed, the traffic does not increase. Processing: Menu 3 in the upper right corner --> Advanced --> Connection options --> Reset encryption Key\n It is best to cooperate with screen to run tasks in the background"
	yellow "1. Run [ALIILAPRO] script"
	yellow "2. Run [mixool] script"
	yellow "3. Back to home directory"
	red "=============================================================="
	read -p "Please enter number:" CHOOSEPLUS
	case "$CHOOSEPLUS" in
		1 ) input
		    [[ $(type -P git) ]] || apt -y install git 2>/dev/null || yum -y install git 2>/dev/null
		    [[ $(type -P python3) ]] || apt -y install python3 2>/dev/null || yum -y install python3 2>/dev/null
		    [[ -d ~/warp-plus-cloudflare ]] || git clone https://github.com/aliilapro/warp-plus-cloudflare.git
		    echo $ID | python3 ~/warp-plus-cloudflare/wp-plus.py;;
		2 ) input
		    read -p "The target flow value you want to get, the unit is GB, just enter a number, the default value is 10:" MISSION
		    wget --no-check-certificate $CDN -N https://cdn.jsdelivr.net/gh/mixool/across/wireguard/warp_plus.sh
		    sed -i "s/eb86bd52-fe28-4f03-a944-60428823540e/$ID/g" warp_plus.sh
		    bash warp_plus.sh $(echo $MISSION | sed 's/[^0-9]*//g');;
		3 ) menu$PLAN;;
		* ) red "Please enter the correct number [1-3]"; sleep 1; plus;;
	esac
	}

# 免费 Warp 账户升级 Warp+ 账户
update() {
	[[ $TRACE4 = plus || $TRACE6 = plus ]] && red " 已经是 WARP+ 账户，不需要升级 " && exit 1
	[[ ! -e /etc/wireguard/wgcf-account.toml ]] && red " 找不到账户文件：/etc/wireguard/wgcf-account.toml，可以卸载后重装，输入 Warp+ License " && exit 1
	[[ ! -e /etc/wireguard/wgcf.conf ]] && red " 找不到配置文件： /etc/wireguard/wgcf.conf，可以卸载后重装，输入 Warp+ License " && exit 1
	[[ -z $LICENSE ]] && read -p " 请输入Warp+ License:" LICENSE
	i=5
	until [[ ${#LICENSE} = 26 ]]
	do
	let i--
	[[ $i = 0 ]] && red "Input errors up to 5 times, the script exits" && exit 1 || read -p "The key should be 26 characters, please re-enter the Warp+ key ($i times remaining):" LICENSE
        done
	cd /etc/wireguard
	sed -i "s#license_key.*#license_key = \"$LICENSE\"#g" wgcf-account.toml &&
	wgcf update > /etc/wireguard/info.log 2>&1 &&
	(wgcf generate >/dev/null 2>&1
	sed -i "2s#.*#$(sed -ne 2p wgcf-profile.conf)#" wgcf.conf
	sed -i "3s#.*#$(sed -ne 3p wgcf-profile.conf)#" wgcf.conf
	sed -i "4s#.*#$(sed -ne 4p wgcf-profile.conf)#" wgcf.conf
	echo $DOWN | sh >/dev/null 2>&1
	net
	[[ $(wget --no-check-certificate -qO- -4 https://www.cloudflare.com/cdn-cgi/trace | grep warp | cut -d= -f2) = plus || $(wget --no-check-certificate -qO- -6 https://www.cloudflare.com/cdn-cgi/trace | grep warp | cut -d= -f2) = plus ]] &&
	green "Has been upgraded to a Warp+ account\n Device name: $(grep name /etc/wireguard/info.log | awk'{ print $NF }')\n Remaining traffic: $(grep Quota /etc/wireguard/info.log | awk'{ print $(NF-1), $NF }')" ) || red "Upgrade failed, Warp+ account error or more than 5 devices have been activated, continue to use free Warp"
	}

# 同步脚本至最新版本
ver(){
	wget -N $CDN -P /etc/wireguard https://cdn.jsdelivr.net/gh/fscarmen/warp/menu.sh &&
	chmod +x /etc/wireguard/menu.sh &&
	ln -sf /etc/wireguard/menu.sh /usr/bin/warp &&
	green "Success! The latest script has been synchronized, version number: $(grep ^VERSION /etc/wireguard/menu.sh | cut -d = -f2) New function: $(grep ^TXT /etc/wireguard/menu.sh | cut -d \" -f2) "|| red" Upgrade failed, problem feedback: [https://github.com/fscarmen/warp/issues] "
	exit
	}
# 单栈
menu1(){
	status
	[[ $IPV4$IPV6 = 01 ]] && green "1. Add IPv4 network interface for IPv6 only VPS" || green "1. Add IPv6 network interface for IPv4 only VPS"
	[[ $IPV4$IPV6 = 01 ]] && green "2. Add IPV4+IPV6 network interface for IPv6 only VPS" || green "2. Add IPV4+IPV6 network interface for IPv4 only VPS"
	[[ $PLAN = 3 ]] && green "3. Turn off WARP temporarily" || green "3. Turn on WARP"
	green "4. Permanently close the WARP network interface and delete WGCF"
	green "5. Upgrade kernel, install BBR, DD script"
	green "6. Get Warp+ traffic"
	green "7. Sync the latest version"
	green "0. Exit the script \n"
	read -p "Please enter a number:" CHOOSE1
		case "$CHOOSE1" in
		1 )	MODIFY=$(eval echo \$MODIFYS$IPV4$IPV6);	install;;
		2 )	MODIFY=$(eval echo \$MODIFYD$IPV4$IPV6);	install;;
		3 )	onoff;;
		4 )	uninstall;;
		5 )	bbrInstall;;
		6 )	plus;;
		7 )	ver;;
		0 )	exit;;
		* )	red "Please enter the correct number [0-7]"; sleep 1; menu1;;
		esac
	}

# 双栈
menu2(){ 
	status
	green "1. Add WARPIPV4+IPV6 network interface to native IPV4+IPV6 VPS"
	[[ $PLAN = 3 ]] && green "2. Turn off WARP temporarily" || green "2. Turn on WARP"
	green "3. Permanently close the WARP network interface and delete the WGCF"
	green "4. Upgrade kernel, install BBR, DD script"
	green "5. Brush Warp+ traffic"
	green "6. Sync the latest version"
	green "0. Exit the script \n"
	read -p "Please enter a number:" CHOOSE2
		case "$CHOOSE2" in
			1) MODIFY=$(eval echo \$MODIFYD$IPV4$IPV6); install;;
			2) onoff;;
			3) uninstall;;
			4) bbrInstall;;
			5) plus;;
			6) ver;;
			0) exit;;
			*) red "Please enter the correct number [0-6]"; sleep 1; menu2;;
		esac
	}

# 已开启 warp 网络接口
menu3(){ 
	status
	[[ $PLAN = 3 ]] && green "1. Turn off WARP temporarily" || green "1. Turn on WARP"
	green "2. Permanently close the WARP network interface and delete WGCF"
	green "3. Upgrade kernel, install BBR, DD script"
	green "4. Brush Warp+ traffic"
	green "5. Upgrade to WARP+ account"
	green "6. Sync the latest version"
	green "0. Exit the script \n"
	read -p "Please enter a number:" CHOOSE3
        case "$CHOOSE3" in
		1 )	onoff;;
		2 )	uninstall;;
		3 )	bbrInstall;;
		4 )	plus;;
		5 )	update;;
		6 )	ver;;
		0 )	exit;;
		* )	red "Please enter the correct number [0-6]"; sleep 1; menu3;;
		esac
	}

# 参数选项 LICENSE
LICENSE=$2

# 参数选项 OPTION：1=为 IPv4 或者 IPv6 补全另一栈Warp; 2=安装双栈 Warp; u=卸载 Warp; b=升级内核、开启BBR及DD; o=Warp开关； p=刷 Warp+ 流量; 其他或空值=菜单界面
OPTION=$1

# 设置后缀
case "$OPTION" in
1 )	[[ $PLAN = 2 ]] && read -p "This system VPS is IPV4+IPV6, you can only choose Warp IPV4+IPV6 plan, please enter y to continue, or exit:" DUAL &&
	[[ $DUAL != [Yy] ]] && exit 1 || MODIFY=$(eval echo \$MODIFYD$IPV4$IPV6)
	[[ $PLAN = 1 ]] && MODIFY=$(eval echo \$MODIFYS$IPV4$IPV6)
 	[[ $PLAN = 3 ]] && yellow "Check that WARP is turned on, and then run the last command to install or enter !!" && echo $DOWN | sh >/dev/null 2>&1 && exit 1
	install;;
2 )	[[ $PLAN = 3 ]] && yellow "Check that WARP is turned on, and then run the last command to install or enter !!" && echo $DOWN | sh >/dev/null 2>&1 && exit 1
	MODIFY=$(eval echo \$MODIFYD$IPV4$IPV6);	install;;
[Bb] )	bbrInstall;;
[Pp] )	plus;;
[Dd] )	update;;
[Uu] )	uninstall;;
[Vv] )	ver;;
[Oo] )	onoff;;
[Nn] )	net;;
[Hh] )	help;;
* )	menu$PLAN;;
esac
